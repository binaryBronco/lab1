package ReadTemperature;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

    /**
     * Note: Lab1 starter code provided by Dr. Sherri Harms of UNK.
     * This program uses a 2D array of temperatures gathered from an input file.
     * The monthly (row) averages and standard deviations are calculated, as
     * well as the yearly (column) averages and standard deviations.
     * @author    John Reagan
     * @version   1.1, 02/04/2021
     */

class ReadTemperature {
    // These attributes will dictate the size of the two dimensional array.
    final static int MONTHS = 12;
    final static int NUMYEARS = 6;

    /**
     * Get the input file from the user.
     * @return Scanner file for input
     * @throws IOException  For issues with input file.
     */
    private static File getInputFile() throws IOException {
        // Get the INPUT filename.
        JFileChooser chooser = new JFileChooser();
        File home = new File(System.getProperty("user.home"));
        chooser.setCurrentDirectory(home);
        int status = chooser.showOpenDialog(null);

        // What will happen if no file is chosen.
        if (status != JFileChooser.APPROVE_OPTION) {
            System.out.println("No File Chosen");
            System.exit(0);
        }
        // Open the file.
        return chooser.getSelectedFile();
    }

    /**
     * @param args
     * @throws IOException  For issues with input file.
     */
    public static void main(String[] args) throws IOException {
        // Call getInputFile() to have the user select the input data file.
        File inputFile = getInputFile();

        // Array of month names so they don't need to be taken from input file.
        String[] months = {"January", "February", "March", "April", "May",
                "June", "July", "August", "September", "October",
                "November", "December"};

        // Store the temperatures for the first day of the month for six years
        // into a two dimensional array, by calling the readFile method
        double[][] temps = readFile(inputFile);

        // Use printf statements to format each column of temperature data.
        System.out.printf("\n%-11s%-48s%-8s%-6s", "Month", "Temperatures",
                "Average", "STDEV");
        for (int row = 0; row < temps.length; row++) {
            System.out.printf("\n%-11s%s", months[row] + ":",
                    getMonthValues(temps, row));
            double avg = getRowAverage(temps[row]);
            System.out.printf("%-8.2f", avg);
            System.out.printf("%-6.2f", getRowStdDev(temps[row], avg));
        }
        // Call getColumnAverage(double[][], int) to print yearly averages.
        System.out.printf("\nColumn (Yearly) Temperatures\n%-11s", "Average");
        for (int i = 0; i < temps[0].length; i++)
            System.out.printf("%-8.2f", getColumnAverage(temps, i));

        // Call getColumnStdDev(double[][], int) to print yearly stdevs.
        System.out.printf("\n%-11s", "STDEV");
        for (int j = 0; j < temps[0].length; j++)
            System.out.printf("%-8.2f", getColumnStdDev(temps, j));

        System.out.println("\n\nThank you.");
    }

    /**
     * Calculate the average of temperatures for a month, stored in a 1D array.
     * @param monthTemps    A month (row) of temperature data.
     * @return average of the temperatures for the row
     */
    private static double getRowAverage(double[] monthTemps) {
        double sum = 0;
        for (double temp : monthTemps)
            sum += temp;
        return sum / monthTemps.length;
    }

    /**
     * Calculate the std devs of temperatures for a month, stored in a 1D array.
     * @param monthTemps    A month (row) of temperature data.
     * @param mean          The mean for this row of temperature data.
     * @return standard deviation of the temperatures for the row
     */
    private static double getRowStdDev(double[] monthTemps, double mean) {
        double stdev = 0;
        for (double temp : monthTemps)
            stdev += Math.pow(temp - mean, 2.0);
        return Math.sqrt(stdev/monthTemps.length);
    }

    /**
     * Calculate the average of temps for a year (column), stored in a 2D array.
     * @param temps2D       Two dimensional array of temperature data.
     * @param col           The column (year) of interest.
     * @return average of the temperatures for the column
     */
    private static double getColumnAverage(double[][] temps2D, int col) {
        double sum = 0;
        for (double[] month : temps2D)
            sum += month[col];
        return sum / temps2D.length;
    }

    /**
     * Calculate the std dev of temps for a year (column), stored in a 2D array.
     * @param temps2D       Two dimensional array of temperature data.
     * @param col           The column (year) of interest.
     * @return average of the temperatures for the column
     */
    private static double getColumnStdDev(double[][] temps2D, int col) {
        double[] year = new double[temps2D.length];
        double sum = 0;

        // Store each temp for a given year in an array for std dev calculation.
        for (int row = 0; row < temps2D.length; row++) {
            year[row] = temps2D[row][col];
            sum += year[row];
        }
        double avg = sum / year.length;

        // Using the column avg, calculate the std dev for the year (column).
        double stdev = 0;
        for (double temp : year)
            stdev += Math.pow(temp - avg, 2.0);
        return Math.sqrt(stdev/year.length);
    }

    /**
     * Output the temperatures for each month (row) in a String
     * @param temps2D       Two dimensional array of temperature data.
     * @param row           The month (row) of interest.
     * @return the values in the specified row as a String
     */
    private static String getMonthValues(double[][] temps2D, int row) {
        String output = "";
        for (int subscript = 0; subscript < temps2D[0].length; subscript++)
            output += String.format("%-8.2f", temps2D[row][subscript]);
        return output;
    }

    /**
     * Read the temperature data into a two-dimensional array.
     * @param inputFile     The inputFile of temperature data.
     * @return a 2D array of temperature data
     * @throws FileNotFoundException    For issues with the inputFile.
     */
    private static double[][] readFile(File inputFile) throws IOException {
        Scanner inputScanner = new Scanner(inputFile);
        double[][] temperatures = new double[MONTHS][NUMYEARS];

        // For each temperature, read the value from the splitData array and
        // store into the correct spot in the 2D array
        for (int row = 0; row < temperatures.length; row++) {
            String monthData = inputScanner.nextLine();
            String[] splitData = monthData.split(",");

            // Subscript 0 of the splitData array has the month name, which
            // isn't wanted. So 'for' loop begins at 1.
            for (int col = 1; col < splitData.length; col++)
                temperatures[row][col-1] = Double.parseDouble(splitData[col]);
        }
        // Closing the input file.
        inputScanner.close();
        return temperatures;
    }
}
